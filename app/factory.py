from flask import Flask
from flask_bootstrap import Bootstrap
import dotenv


def create_app():
    app = Flask(__name__)
    dotenv.load_dotenv()
    app.config.from_object("config")
    bootstrap = Bootstrap(app)
    
    from app.endpoints import endpoints
    app.register_blueprint(endpoints)
    return app