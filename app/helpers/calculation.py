def get_place_acustic(wall_coefficient, weight, height, depth, roof_coefficient, 
door_coefficient, window_coefficient, windows, doors, furnitures):
    sound_absorption = (wall_coefficient * (weight * (height + depth) - 2.25 * (doors + windows)) + roof_coefficient
     * depth * weight + door_coefficient * 2.25 * doors + window_coefficient * windows * 2.25 * + furnitures)

    current_acustic = 0.16 * (weight * height *depth) / sound_absorption
    right_acustic = ((weight * height * depth) + 4166.5) / 8333
    return current_acustic, right_acustic, sound_absorption

def get_insulation_surface(weight, height, depth, right_acustic, sound_absorption, material_coefficient):
    return (0.16 * weight * height * depth / right_acustic - sound_absorption) / material_coefficient

def get_tiles_amount_needed(material_weight, material_height, insulation_surface):
    return round(insulation_surface / (material_weight * material_height))

# def get_max_insulation_tiles(weight, depth, doors, windows, material_weight):
#     return round(weight * 2 + depth * 2 - doors * 1.04 - windows * 1.5) / material_weight

def get_max_insulation_tiles(weight, depth, height, doors, windows, material_weight, material_height):
    return (weight * 2 - doors * 1.04 - windows * 1.5) / material_weight * height/material_height + depth * 2 / material_weight * height/material_height

def get_price(tiles, price):
    return tiles * price

# def get_new_place_acustic(sound_absorption, material_amount, material_coefficient, weight, height, depth, material_weight, material_height):
#     return 0.16 * weight * height * depth / (sound_absorption - material_amount * material_coefficient * material_weight * material_height)
