from flask import Blueprint, render_template, flash

from flask_wtf import FlaskForm

from app.forms.getDataForm import GetDataForm
from app.helpers.calculation import (get_place_acustic, get_insulation_surface, 
get_tiles_amount_needed, get_max_insulation_tiles, get_price)
from app.helpers.materials import MATERIALS, INSULATION_MATERIALS
endpoints = Blueprint("endpoints", __name__, template_folder='templates')


@endpoints.route('/', methods=["GET", "POST"])
def home():
    form = GetDataForm()
    if form.validate_on_submit():
        door_coefficient = 0.1
        window_coefficient = 0.07
        insulation_surface = {}
        tiles_amount_needed = {}
        max_insulation_tiles = {}
        new_place_acustic = {}
        prices = {}
        if int(form.roof_material.data) == 0:  # madera
            roof_coefficient = 0.06
        elif int(form.roof_material.data) == 1:  # gypsum
            roof_coefficient = 0.09
        elif int(form.roof_material.data) == 2:  # cemento
            roof_coefficient = 0.2

        if int(form.wall_material.data) == 0:  # madera
            wall_coefficient = 0.06
        elif int(form.wall_material.data) == 1:  # gypsum
            wall_coefficient = 0.09
        elif int(form.wall_material.data) == 2:  # cemento
            wall_coefficient = 0.2

        if int(form.furniture_density.data) == 0:  # poco
            furniture_coefficient = 1
        elif int(form.furniture_density.data) == 1:  # normal
            furniture_coefficient = 3
        elif int(form.furniture_density.data) == 2:  # denso
            furniture_coefficient = 7
        
        current_acustic, right_acustic, sound_absorption = get_place_acustic(wall_coefficient, 
        int(form.weight.data), int(form.height.data), int(form.depth.data), roof_coefficient, 
        door_coefficient, window_coefficient, int(form.windows.data), int(form.doors.data), furniture_coefficient)
        is_improve_needed = current_acustic > right_acustic
        
        for material in INSULATION_MATERIALS:
            insulation_surface[material[0]] = get_insulation_surface(int(form.weight.data), int(form.height.data), int(form.depth.data), right_acustic, sound_absorption, material[1])
        
        for material in INSULATION_MATERIALS:
            tiles_amount_needed[material[0]] = get_tiles_amount_needed(material[2], material[3], insulation_surface[material[0]])
        
        for material in INSULATION_MATERIALS:
            max_insulation_tiles[material[0]] = get_max_insulation_tiles(int(form.weight.data), int(form.depth.data), int(form.height.data), int(form.doors.data), int(form.windows.data), material[2], material[3])
        
        for material in INSULATION_MATERIALS:
            if tiles_amount_needed[material[0]] <= max_insulation_tiles[material[0]]:
                prices[material[0]] = get_price(tiles_amount_needed[material[0]], material[4])
            else:
                prices[material[0]] = -1
            
        # for material in INSULATION_MATERIALS:
        #     new_place_acustic[material[0]] = get_new_place_acustic(sound_absorption, tiles_amount_needed[material[0]], material[1], int(form.weight.data), int(form.height.data), int(form.depth.data))
        
        if is_improve_needed:
            return render_template('results.html', current_acustic=current_acustic, right_acustic=right_acustic, 
            tiles_amount_needed=tiles_amount_needed, prices=prices)
        else:
            flash(f"El nivel de acústica de su recinto es:{current_acustic:.2f} y el nivel ideal es: {right_acustic:.2f}, por tanto usted no necesita mejorar la acústica del recinto.", "success")
            
            
    return render_template('main.html', form=form, title="Información del recinto")