from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField
from wtforms.validators import InputRequired

from app.helpers.materials import MATERIALS


class GetDataForm(FlaskForm):
    weight = SelectField('Ancho (m)', validators=[InputRequired()], choices=[(i, i) for i in range(2, 30)])
    height = SelectField('Altura (m)', validators=[InputRequired()], choices=[(i, i) for i in range(2, 30)])
    depth = SelectField('Profundidad (m)', validators=[InputRequired()], choices=[(i, i) for i in range(2, 30)])
    wall_material = SelectField('Material de la pared', validators=[InputRequired()], choices=[(i, name) for i, name in enumerate(MATERIALS)])
    roof_material = SelectField('Material del techo', validators=[InputRequired()], choices=[(i, name) for i, name in enumerate(MATERIALS)])
    furniture_density = SelectField('Muebles', validators=[InputRequired()], choices=[(i, name) for i, name in enumerate(["poco", "normal", "denso"])])
    doors = SelectField('Cantidad de puertas', validators=[InputRequired()], choices=[(i, i) for i in range(1, 6)])
    windows = SelectField('Cantidad de ventanas', validators=[InputRequired()], choices=[(i, i) for i in range(0, 6)])
    submit = SubmitField("Enviar")
